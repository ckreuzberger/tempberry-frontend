# tempBerry Angular 2+ Temperature Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.


## Quickstart

```bash
docker-compose build
docker-compose run --rm node npm install
docker-compose up
```

## Angular Commands

The following commands are the basic angular commands, which should be ran using ``docker-compose run --rm node``. If you want, you can create yourself an alias like this:
```bash
alias ng="docker-compose run --rm node ng"
alias npm="docker-compose run --rm node npm"

```

### Development server

Run  ``docker-compose up`` to run a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


# License

MIT