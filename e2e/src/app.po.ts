import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    browser.waitForAngularEnabled(false);
    return browser.get('/', 1000);
  }

  getParagraphText() {
    return element(by.css('app-root h2 a')).getText();
  }
}
