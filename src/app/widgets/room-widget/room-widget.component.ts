import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-room-widget',
  templateUrl: './room-widget.component.html',
  styleUrls: ['./room-widget.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoomWidgetComponent implements OnInit {
  @Input() room;

  constructor() { }

  ngOnInit() {
  }

}
