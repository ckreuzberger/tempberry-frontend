import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-room-temperature-widget',
  templateUrl: './room-temperature-widget.component.html',
  styleUrls: ['./room-temperature-widget.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoomTemperatureWidgetComponent implements OnInit {
  @Input() room;

  constructor() { }

  ngOnInit() {
  }

}
