import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomTemperatureWidgetComponent } from './room-temperature-widget.component';
import { UIRouterModule } from '@uirouter/angular';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('RoomTemperatureWidgetComponent', () => {
  let component: RoomTemperatureWidgetComponent;
  let fixture: ComponentFixture<RoomTemperatureWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomTemperatureWidgetComponent ],
      imports: [
        UIRouterModule.forRoot({useHash: true}),
        FontAwesomeModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomTemperatureWidgetComponent);
    component = fixture.componentInstance;
    component.room = { id: 1, name: "Test Room" };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
