import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { RoomTemperatureWidgetComponent } from './room-temperature-widget/room-temperature-widget.component';
import { UIRouterModule } from '@uirouter/angular';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RoomTemperatureChartWidgetComponent } from './room-temperature-chart-widget/room-temperature-chart-widget.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NvD3Module } from 'ng2-nvd3';

// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';
import { RoomWidgetComponent } from './room-widget/room-widget.component';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    UIRouterModule,
    FontAwesomeModule,
    NvD3Module,
  ],
  declarations: [RoomTemperatureWidgetComponent, RoomTemperatureChartWidgetComponent, RoomWidgetComponent],
  exports: [
    RoomWidgetComponent,
    RoomTemperatureWidgetComponent,
    RoomTemperatureChartWidgetComponent
  ]
})
export class WidgetsModule { 
  
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: WidgetsModule,
      providers: []
    };
  }
}
