import { Component, OnInit, Input, ViewEncapsulation, ChangeDetectionStrategy, OnChanges } from '@angular/core';

declare let d3: any;


@Component({
  selector: 'app-room-temperature-chart-widget',
  templateUrl: './room-temperature-chart-widget.component.html',
  styleUrls: [
    // include nvd3 styles
    '../../../../node_modules/nvd3/build/nv.d3.css',
    './room-temperature-chart-widget.component.css'
  ],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoomTemperatureChartWidgetComponent implements OnInit, OnChanges {
  @Input()
  roomHistoryData: any[];

  options;

  constructor() { }

  ngOnChanges() {
    console.log('In ngOnChanges');
    // update yDomain
    let ymin = 10;
    let ymax = 50;


    for (var i = 0; i < this.roomHistoryData.length; i++) {
      for (var j = 0; j < this.roomHistoryData[i].values.length; j++) {
        const value = this.roomHistoryData[i].values[j].value;

        if (value > ymax) {
          ymax = value + 1;
        }

        if (value < ymin) {
          ymin = value - 1;
        }
      }
    }

    if (!this.options) {
      this.initChart();
    }

    this.options.chart.yDomain = [ymin, ymax];
  }

  private initChart() {
    this.options = {
      chart: {
        type: 'lineChart',
        height: '400',
        margin: {
          top: 20,
          right: 20,
          bottom: 100,
          left: 55
        },
        x: function (d) { 
          return d.label; 
        },
        y: function (d) { 
          return d.value;
         },
        useInteractiveGuideline: true,
        yDomain: [10, 80],
        xAxis: {
          axisLabel: 'Time (h)',
          tickFormat: function (d) {
            return d3.time.format('%x %H:%M')(new Date(d))
          },
          rotateLabels: 45,
          showMaxMin: false
        },
        yAxis1: {
          axisLabel: 'Temperature (°C)',
          tickFormat: function (d) {
            return d3.format('.01f')(d);
          },
          axisLabelDistance: -10
        },
        yAxis2: {
          axisLabel: 'Relative Humidity (%)',
          tickFormat: function (d) {
            return d3.format('.01f')(d);
          },
          axisLabelDistance: -10
        },
        callback: function (chart) {
          console.log("!!! lineChart callback !!!");
        }
      },
      title: {
        enable: true,
        text: 'Last 3 days'
      },
    };
  }

  ngOnInit() {
  }
}
