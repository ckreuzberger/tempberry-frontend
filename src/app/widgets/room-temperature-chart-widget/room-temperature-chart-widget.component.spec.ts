import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NvD3Module } from 'ng2-nvd3';

import { RoomTemperatureChartWidgetComponent } from './room-temperature-chart-widget.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('RoomTemperatureChartWidgetComponent', () => {
  let component: RoomTemperatureChartWidgetComponent;
  let fixture: ComponentFixture<RoomTemperatureChartWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomTemperatureChartWidgetComponent ],
      imports: [
        NvD3Module,
        BrowserAnimationsModule, 
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomTemperatureChartWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
