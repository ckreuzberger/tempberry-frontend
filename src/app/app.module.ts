import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule, Component, Injectable, ErrorHandler } from '@angular/core';
import { UIRouterModule } from "@uirouter/angular";
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas, faCoffee, faBatteryQuarter, faCaretDown, faCaretUp, faThermometerHalf, faTint, faCloud } from '@fortawesome/free-solid-svg-icons';
import { far, faFrownOpen } from '@fortawesome/free-regular-svg-icons';


import * as Sentry from "@sentry/browser";

import { AppComponent } from '@app/app.component';
import { ScreensModule, states } from '@app/screens/screens.module';
import { ServicesModule } from '@app/services/services.module';
import { WidgetsModule } from '@app/widgets/widgets.module';
import { environment } from '@environment/environment';
import { FormsModule } from '@angular/forms';


import { NvD3Module } from 'ng2-nvd3';

// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';
import { fakeBackendProvider } from './services/fakeBackend/fakeBackend';
import { ServiceWorkerModule } from '@angular/service-worker';


/** States */
const routingStates = [
  ...states
];


@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error) {
    Sentry.captureException(error.originalError || error);
    throw error;
  }
}

/** Configure Sentry */
if (environment.sentryDsn) {
  Sentry.init({
    dsn: environment.sentryDsn
  });
}



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ServicesModule.forRoot(),
    WidgetsModule.forRoot(),
    ScreensModule.forRoot(),
    UIRouterModule.forRoot({ states: routingStates, useHash: true }),
    NgbModule,
    FontAwesomeModule,
    NvD3Module,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    // add sentry error handler provider
    { provide: ErrorHandler, useClass: SentryErrorHandler },
    // fake backend
    fakeBackendProvider,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    // fontawesome icons

    library.addIconPacks(fas);
    library.addIconPacks(far);

    // Add an icon to the library for convenient access in other components
    library.addIcons(faCoffee);
    library.addIcons(faBatteryQuarter);
    library.addIcons(faCaretUp);
    library.addIcons(faCaretDown);
    library.addIcons(faFrownOpen);
    library.addIcons(faThermometerHalf, faTint, faCloud);

  }
 }
