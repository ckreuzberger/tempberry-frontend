import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


export class RoomHistoryItem {

  id: number;
  sensor_id: number;
  temperature: number | null;
  humidity: number | null;
  air_pressure: number | null;
  created_at: string;
  room: number;
  source: string;
  battery: number;

/**
 *   "id": 4682056,
  "sensor_id": 31,
  "temperature": 22.2,
  "humidity": 57.0,
  "air_pressure": null,
  "created_at": "2018-10-10T13:57:12Z",
  "room": 4,
  "source": "raspberry",
  "battery": 1
 */
}


@Injectable({
  providedIn: 'root'
})
export class RoomsRestService {
  private baseURL = "https://tempberry.chkr.at/api/rooms/";
  private baseURLTemperatures = "https://tempberry.chkr.at/api/temperatures/";

  constructor(private http: HttpClient) {
  }

  public getLatestData() : Observable<any[]> {
    return this.http.get<any[]>(this.baseURL + "latest/");
  }

  public getAggregates24Hours(roomPk): Observable<any> {
    return this.http.get<any>(this.baseURL + roomPk + "/aggregates_24h/");
  }

  public getTemperatureHistory(roomPk, sensorId): Observable<RoomHistoryItem[]> {
    return this.http.get<RoomHistoryItem[]>(this.baseURLTemperatures + "?room_id=" + roomPk + "&sensor_id=" + sensorId);
  }
}
