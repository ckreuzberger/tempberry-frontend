import { TestBed, inject } from '@angular/core/testing';

import { RoomsRestService } from './rooms-rest.service';
import { HttpClientModule } from '@angular/common/http';

describe('RoomsRestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoomsRestService],
      imports: [
        HttpClientModule
      ]
    });
  });

  it('should be created', inject([RoomsRestService], (service: RoomsRestService) => {
    expect(service).toBeTruthy();
  }));
});
