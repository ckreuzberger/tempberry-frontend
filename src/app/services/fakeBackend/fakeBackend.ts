import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs-compat/add/observable/of';
import 'rxjs-compat/add/operator/mergeMap';
import 'rxjs-compat/add/observable/throw';
import 'rxjs-compat/add/operator/materialize';
import 'rxjs-compat/add/operator/delay';
import 'rxjs-compat/add/operator/dematerialize';

import { environment } from '@environment/environment';


@Injectable({
    providedIn: 'root'
})
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (environment.fakeBackend) {
            // wrap in delayed observable to simulate server api call
            return Observable.of(null).mergeMap(() => {
                console.log(`Fake Backend | ${request.method}: ${request.url}, Body: ${JSON.stringify(request.body)}`);

                if (request.url.endsWith('/foo')) {

                }

                // tempberry rooms
                if (request.url.endsWith('/api/rooms/latest/') && request.method === 'GET') {
                    const rooms = [
                        {
                            "id": 4, "name": "Arbeitszimmer", "comment": "", "created_at": "2017-09-30T16:20:18Z", "public": true,
                            "live_data": { "id": 4965256, "sensor_id": 31, "temperature": 22.8, "humidity": 56.0, "air_pressure": null, "created_at": "2018-11-13T19:38:22.947713Z", "room": 4, "source": "raspberry", "battery": 1 }, "has_temperature": true, "has_humidity": true, "has_air_pressure": false, "average_last_hour": { "air_pressure": null, "temperature": 22.61643835616439, "humidity": 55.821917808219176 }
                        }, {
                            "id": 2, "name": "Außensensor", "comment": "Beim Küchenfenster", "created_at": "2017-09-30T17:25:36Z", "public": true,
                            "live_data": { "id": 4965254, "sensor_id": 93, "temperature": 7.4, "humidity": 68.0, "air_pressure": null, "created_at": "2018-11-13T19:38:07.977777Z", "room": 2, "source": "raspberry", "battery": 1 }, "has_temperature": true, "has_humidity": true, "has_air_pressure": false, "average_last_hour": { "air_pressure": null, "temperature": 7.77945205479452, "humidity": 109.89041095890411 }
                        },
                        {
                            "id": 1, "name": "Gästezimmer", "comment": "Luftfeuchtigkeits-Sensor defekt", "created_at": "2017-09-30T17:25:40Z", "public": true,
                            "live_data": { "id": 4965255, "sensor_id": 110, "temperature": 21.6, "humidity": 10.0, "air_pressure": null, "created_at": "2018-11-13T19:38:16.505269Z", "room": 1, "source": "raspberry", "battery": 0 }, "has_temperature": true, "has_humidity": false, "has_air_pressure": false, "average_last_hour": { "air_pressure": null, "temperature": 21.63134328358208, "humidity": 10.0 }
                        }
                    ];

                    return Observable.of(new HttpResponse({ status: 200, body: rooms }));
                }

                return Observable.of(new HttpResponse({ status: 200, body: {} }));

                console.log(`Fake Backend | ${request.method}: ${request.url} not processed -> going to real api`);

                // pass through any requests not handled above
                return next.handle(request);
            })

                // call materialize and dematerialize to ensure delay even if an error
                // is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
                .materialize()
                .delay(500)
                .dematerialize();
        }
        // else: do not do anything with fake backend
        return next.handle(request);
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
