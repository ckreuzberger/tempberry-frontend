import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomDetailComponent } from './room-detail.component';
import { ServicesModule } from '../../services/services.module';
import { WidgetsModule } from '@app/widgets/widgets.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('RoomDetailComponent', () => {
  let component: RoomDetailComponent;
  let fixture: ComponentFixture<RoomDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomDetailComponent ],
      imports: [
        // import services module and all its dependencies
        ServicesModule,
        WidgetsModule,
        FontAwesomeModule,
      ],
      providers: [
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomDetailComponent);
    component = fixture.componentInstance;
    component.roomPk = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
