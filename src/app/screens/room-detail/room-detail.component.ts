import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription, interval, Observable } from 'rxjs';
import { RoomsRestService } from '@app/services/rooms/rooms-rest.service';
import { from } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';


@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.css']
})
export class RoomDetailComponent implements OnInit, OnDestroy {
  @Input()
  roomPk;

  room: any;

  roomStats: any;

  loaded: boolean = false;
  historyLoaded: boolean = false;

  roomHistoryData: {};

  intervalSubscribtion: Subscription;

  constructor(private roomsRestService: RoomsRestService) { }

  ngOnInit() {

    this.getLatestData().pipe(
      map((room) => this.getHistory()),
      map((room) => this.getStats())
    ).subscribe();

    // call get latest data regularly (every 10 seconds)
    this.intervalSubscribtion = interval(10 * 1000).subscribe(
      val => {
        this.getLatestData().subscribe();
      }
    );
  }

  ngOnDestroy() {
    this.intervalSubscribtion.unsubscribe();
  }

  private average(input: any[]) {
    let output = 0;
    for (let i = 0; i < input.length; i++) {
      output+=Number(input[i]);
    }
    return output/input.length;
  };

  private getStats() {
    this.roomsRestService.getAggregates24Hours(this.roomPk).subscribe(
      roomStats => {
        this.roomStats = roomStats;
      }
    )
  }

  private queryHistory(roomPk: number, sensorId: number) {
    console.log("Querying history")
    this.roomsRestService.getTemperatureHistory(roomPk, sensorId).subscribe(
      roomHistory => {
        let temperatures = [],
            humidities = [];

        let min_y = 99,
            max_y = -99;

        let last_temperatures = [],
            last_humidities = [];

        // iterate over roomHistory
        for (let i = 0; i < roomHistory.length; i++) {
          // check min/max values
          if (roomHistory[i].temperature > max_y) {
            max_y = roomHistory[i].temperature;
          }
          if (roomHistory[i].temperature < min_y) {
            min_y = roomHistory[i].temperature;
          }
          if (roomHistory[i].humidity > max_y) {
            max_y = roomHistory[i].humidity;
          }
          if (roomHistory[i].humidity < min_y) {
            min_y = roomHistory[i].humidity;
          }

          // collect those values for average calculation
          last_temperatures.push(roomHistory[i].temperature);
          last_humidities.push(roomHistory[i].humidity);

          // only take every 5th value (so approx. every 5th minute)
          if (i % 5 == 4) {
            // push the average temperature of the last 5 entries to temperatures array
            temperatures.push(
              {
                label: Date.parse(roomHistory[i - 2].created_at),
                value: Math.round(10 * this.average(last_temperatures)) / 10
              }
            );
            // push the average humidity of the last 5 entries to humidity array
            humidities.push(
              {
                label: Date.parse(roomHistory[i - 2].created_at),
                value: Math.round(this.average(last_humidities))
              }
            );

            // clean last_temperatures and last_humidities
            last_humidities.length = 0;
            last_temperatures.length = 0;
          }
        }
        // set charts min/max values
        // vm.chartOptions.chart.yDomain = [Math.floor(min_y) - 5, Math.ceil(max_y) + 5];

        // clear the chart data
        // vm.data = [];

        let chartData = [];

        // add room temperature data
        if (this.room.has_temperature) {
          chartData.push(
            {
              yAxis: 1,
              values: temperatures,      //values - represents the array of {x,y} data points
              key: 'Temperatures', //key  - the name of the series
              color: '#ff7f0e',
              strokeWidth: 2,
              classed: 'dashed'
            }
          );
        }

        // add room humidity data
        if (this.room.has_humidity) {
          chartData.push(
            {
              yAxis: 2,
              values: humidities,      //values - represents the array of {x,y} data points
              key: 'Humidity', //key  - the name of the series.
              color: '#135dab',
              strokeWidth: 2,
              classed: 'dashed'
            }
          )
        }

        console.log("Setting roomHistoryData to sensorId=" + sensorId + " to chart data")
        this.roomHistoryData[sensorId] = chartData;

        this.historyLoaded = true;
      }
    )
  }

  private getHistory() {
    this.historyLoaded = false;
    this.roomHistoryData = {};
    for (let sensor of this.room.sensors) {
      if (sensor.live_data?.sensor_id) {
        this.queryHistory(this.room.id, sensor.live_data?.sensor_id);
      }
    }
  }

  private getLatestData() {    

    let myPromise = this.roomsRestService.getLatestData();

    return myPromise.pipe(map(rooms => {
        this.loaded = true;
        // iterate over rooms and find the one with pk=roomid
        for (let room of rooms) {
          if (room.id == this.roomPk) {
            // found
            this.loaded = true;
            this.room = room;
            console.log("Room found!")
            return room;
          }
        }
      })
    )
  }
}
