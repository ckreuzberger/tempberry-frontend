import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { ServicesModule } from '../../services/services.module';
import { UIRouterModule } from '@uirouter/angular';
import { WidgetsModule } from '@app/widgets/widgets.module';


describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardComponent ],
      imports: [
        // import services module and all its dependencies
        ServicesModule,
        // import UI Router Module for ui-sref functionality
        UIRouterModule,
        // import Widgets MOdule for widgets
        WidgetsModule,
      ],
      providers: [
        // { provide: XhrBackend, useClass: MockBackend }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
