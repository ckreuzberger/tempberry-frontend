import { Component, OnInit, OnDestroy } from '@angular/core';

import { RoomsRestService } from '@app/services/rooms/rooms-rest.service';

import { interval, Subscription } from 'rxjs';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  rooms: any[];
  loaded: boolean = false;
  intervalSubscribtion: Subscription;

  constructor(private roomsRestService: RoomsRestService) { }

  ngOnInit() {
    this.getLatestData();

    // call get latest data regularly (every 10 seconds)
    this.intervalSubscribtion = interval(10 * 1000).subscribe(
      val => {
        this.getLatestData();
      }
    );
  }

  ngOnDestroy() {
    this.intervalSubscribtion.unsubscribe();
  }

  private getLatestData() {    
    this.roomsRestService.getLatestData().subscribe(
      rooms => {
        this.loaded = true;
        this.rooms = rooms;
      }
    )
  }
}
