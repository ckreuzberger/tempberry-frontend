import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from '@app/screens/dashboard/dashboard.component';
import { FormsModule } from '@angular/forms';
import { RoomDetailComponent } from './room-detail/room-detail.component';
import { Transition } from '@uirouter/core';
import { UIRouterModule } from '@uirouter/angular';
import { WidgetsModule } from '@app/widgets/widgets.module';
import { ServicesModule } from '@app/services/services.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// define states for routing
let dashboardState = { name: 'dashboard', url: '',  component: DashboardComponent }; 

export function resolveRoomPk(trans) {
  return trans.params().roomPk;
}

let roomDetailState = { 
  name: 'roomDetail', url: '/room/:roomPk', component: RoomDetailComponent,
  resolve: [
    {
      token: 'roomPk',
      deps: [Transition, ],
      resolveFn: resolveRoomPk
    }
  ]
};

export const states = [
  dashboardState,
  roomDetailState
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UIRouterModule,
    WidgetsModule,
    ServicesModule,
    FontAwesomeModule,
  ],
  exports: [],
  declarations: [
    // components that are declared within screens
    DashboardComponent,
    RoomDetailComponent,
  ]
})
export class ScreensModule {

  static forRoot(): ModuleWithProviders<ScreensModule> {
    return {
      ngModule: ScreensModule,
      providers: []
    };
  }
}
