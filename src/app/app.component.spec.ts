import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { UIRouterModule } from '@uirouter/angular';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        // { provide: XHRBackend, useClass: MockBackend }
      ],
      imports: [
          // need to import ui router here
          UIRouterModule.forRoot({useHash: true})
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
