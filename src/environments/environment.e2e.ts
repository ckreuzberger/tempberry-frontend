
export const environment = {
    production: false,
    sentryDsn: null,
    fakeBackend: true, // set to false for the real backend
  };
  